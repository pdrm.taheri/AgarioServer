cmake_minimum_required(VERSION 3.7)
project(AgarioServer)

set(CMAKE_CXX_STANDARD 11)

set(CMAKE_AUTOMOC ON)

set(CMAKE_PREFIX_PATH /opt/Qt5.8.0/5.8/gcc_64/)

# Find Qt libraries needed
find_package(Qt5Network)
find_package(Qt5Widgets)

set(SOURCE_FILES main.cpp
        main.cpp
        Model/GameWorld/GameObject.cpp
        Model/GameWorld/GameObject.h
        Model/GameWorld/Food.cpp
        Model/GameWorld/Food.h
        Model/GameWorld/Player.cpp
        Model/GameWorld/Player.h
        Model/GameWorld/AIPlayer.cpp
        Model/GameWorld/AIPlayer.h
        Model/GameWorld/HumanPlayer.cpp
        Model/GameWorld/HumanPlayer.h
        Model/GameWorld/GameWorld.cpp
        Model/GameWorld/GameWorld.h
        Model/GameWorld/Hideout.cpp
        Model/GameWorld/Hideout.h
        Model/GameWorld/GameWorldDataStore.cpp
        Model/GameWorld/GameWorldDataStore.h
        Model/Geometry/Circle.cpp
        Model/Geometry/Circle.h
        Model/Geometry/Shape.cpp
        Model/Geometry/Shape.h
        Model/Geometry/Point.cpp
        Model/Geometry/Point.h
        Model/Geometry/Vector.cpp
        Model/Geometry/Vector.h
        Model/Connection/ConnectionDataStore.cpp
        Model/Connection/ConnectionDataStore.h
        Controller/Server.cpp
        Controller/Server.h
        Controller/Connection/ConnectionHandler.cpp
        Controller/Connection/ConnectionHandler.h
        Controller/Connection/Connection.h
        Controller/Connection/Connection.cpp
        Constants.h Controller/Parser.cpp Controller/Parser.h Model/Geometry/Utility.cpp Model/Geometry/Utility.h)

add_executable(AgarioServer ${SOURCE_FILES})

target_link_libraries(AgarioServer Qt5::Network)
target_link_libraries(AgarioServer Qt5::Widgets)