//
// Created by pedram on 3/14/17.
//

#include <cmath>
#include <set>
#include "Server.h"
#include "../Model/Geometry/Utility.h"

// Definition of static variables
static Server *instance;

Server::Server()
{
    ConnectionHandler::getInstance()->setOnConnectionClosedHandler(Server::onClientDisconnected);
    ConnectionHandler::getInstance()->setOnNewConnectionHandler(Server::onNewClientConnect);
    ConnectionHandler::getInstance()->setOnNewMessageHandler(Server::onNewMessage);
}

Server::~Server()
{
    delete instance;
}

void Server::onNewClientConnect(Connection *newConnection)
{
    qDebug() << "[SERVER] New connection!";
    newConnection->setId(ConnectionDataStore::getInstance()->getConnections().size());

    GameWorldDataStore::getInstance()->getPlayers().emplace_back(new Player(newConnection->getId()));

    QString serialized = Parser::serializeGameWorld(GameWorldDataStore::getInstance()) + "\n";
    qDebug() << newConnection->getId();
    qDebug() << ('S' + QString::number(newConnection->getId()) + serialized);
    newConnection->getSocket()->write(('S' + QString::number(newConnection->getId()) + serialized).toStdString().c_str());

    ConnectionDataStore::getInstance()->getConnections().emplace_back(std::move(newConnection));
}

void Server::onNewMessage(int senderId, QByteArray message)
{
    qDebug() << "[SERVER] New message: " << message;

    qDebug() << GameWorldDataStore::getInstance()->getPlayers()[senderId]->boost;
    if (message.toStdString() == "Shoot\n")
    {
        double r1 = GameWorldDataStore::getInstance()->getPlayers()[senderId]->getShape()->getRadius();
        if (r1 > 30)
        {
            GameWorldDataStore::getInstance()->getPlayers()[senderId]->boost = std::max(
                    GameWorldDataStore::getInstance()->getPlayers()[senderId]->boost, 2.5);

            Player *p = new Player(-1);
            p->getShape()->setRadius(10);
            p->brake = 0.95;
            p->getShape()->setCentre(GameWorldDataStore::getInstance()->getPlayers()[senderId]->getShape()->getCentre());
            auto speed = GameWorldDataStore::getInstance()->getPlayers()[senderId]->getSpeed();
            speed.setX(speed.getX() * -1);
            speed.setY(speed.getY() * -1);
            p->setSpeed(speed);
            while (Utility::canEat(p, GameWorldDataStore::getInstance()->getPlayers()[senderId]))
            {
                p->setSpeed(speed);
                p->move();
            }
            GameWorldDataStore::getInstance()->getPlayers().emplace_back(p);

            double r2 = p->getShape()->getRadius();
            GameWorldDataStore::getInstance()->getPlayers()[senderId]->getShape()->setRadius(sqrt(r1 * r1 - r2 * r2));
        }
    }
    else
    {
        updatePlayersDynamics(senderId, Parser::praseMessageFromByteArray(message));
    }
}

void Server::onClientDisconnected(Connection *connection)
{
    std::vector<Connection *>::iterator iter = std::find(ConnectionDataStore::getInstance()->getConnections().begin(),
                                                         ConnectionDataStore::getInstance()->getConnections().end(),
                                                         connection);

    qDebug() << "Client about disconnect with ID: " << (*iter)->getId();

    auto indexOf = iter - ConnectionDataStore::getInstance()->getConnections().begin();
    ConnectionDataStore::getInstance()->getConnections().erase(iter);

    GameWorldDataStore::getInstance()->getPlayers().erase(GameWorldDataStore::getInstance()->getPlayers().begin() + indexOf);
    delete connection;

    updateIdsAfterDisconnection();
}

void Server::updateIdsAfterDisconnection()
{
    int i = 0;
    for (auto &c : ConnectionDataStore::getInstance()->getConnections())
    {
        c->setId(i++);
    }

    i = 0;
    for (auto &p : GameWorldDataStore::getInstance()->getPlayers())
    {
        if (p->getId() == -1)
            continue;

        p->setId(i++);
    }
}

Server *Server::getInstance()
{
    if (instance == nullptr)
    {
        instance = new Server();
    }

    return instance;
}

void Server::run()
{
    getInstance(); // To initialize handlers

    QHostAddress address(HOST_ADDRESS);
    ConnectionHandler::getInstance()->listenOn(address, HOST_PORT);

    getInstance()->startTimer(GAME_CYCLE_IN_MILLISECONDS);

    for (int i = 0; i < 500; ++i)
    {
        GameWorldDataStore::getInstance()->getFoods().push_back(new Food());
    }
}

void Server::updatePlayersDynamics(int playerId, std::pair<int, int> mouse)
{
    QPointF loc = GameWorldDataStore::getInstance()->getPlayers()[playerId]->getShape()->getCentre();
    Vector speed(mouse.first - loc.x(), mouse.second - loc.y());
    GameWorldDataStore::getInstance()->getPlayers()[playerId]->setSpeed(speed.normalized());
}

void Server::updatePlayersPositions(int playerId)
{
    GameWorldDataStore::getInstance()->getPlayers()[playerId]->move();
}

void Server::update()
{
    for (int i = 0; i < GameWorldDataStore::getInstance()->getPlayers().size(); ++i)
        updatePlayersPositions(i);

    // Deaths and stuff
    calculateDeathsAndUpdateWeights();

    // Finish the cycle by uploading data to clients
    sendDataToClients();
}

void Server::calculateDeathsAndUpdateWeights()
{
    std::set<int> deadPlayersIndices;
    std::set<int> deadPlayersIds;
    for (int i = 0; i < GameWorldDataStore::getInstance()->getPlayers().size(); ++i)
    {
        // Other Players
        for (int j = 0; j <= i; ++j)
        {
            Player *eater = Utility::canEat(GameWorldDataStore::getInstance()->getPlayers()[i], GameWorldDataStore::getInstance()->getPlayers()[j]);
            if (eater == nullptr)
                continue;

            double r1 = GameWorldDataStore::getInstance()->getPlayers()[i]->getShape()->getRadius();
            double r2 = GameWorldDataStore::getInstance()->getPlayers()[j]->getShape()->getRadius();

            eater->getShape()->setRadius(round(sqrt(r1 * r1 + r2 * r2)));

            if (GameWorldDataStore::getInstance()->getPlayers()[i] == eater)
            {
                deadPlayersIndices.insert(j);
                if (GameWorldDataStore::getInstance()->getPlayers()[j]->getId() != -1)
                    deadPlayersIds.insert(GameWorldDataStore::getInstance()->getPlayers()[j]->getId());
            }
            else
            {
                deadPlayersIndices.insert(i);
                if (GameWorldDataStore::getInstance()->getPlayers()[i]->getId() != -1)
                    deadPlayersIds.insert(GameWorldDataStore::getInstance()->getPlayers()[i]->getId());
            }
        }

        // Food
        for (int j = 0; j < GameWorldDataStore::getInstance()->getFoods().size(); ++j)
        {
            // Continue if they're far apart
            if (!Utility::canEat(GameWorldDataStore::getInstance()->getPlayers()[i], GameWorldDataStore::getInstance()->getFoods()[j]))
                continue;


            double r1 = GameWorldDataStore::getInstance()->getPlayers()[i]->getShape()->getRadius();
            double r2 = GameWorldDataStore::getInstance()->getFoods()[j]->getShape()->getRadius();

            // Increase player's size
            GameWorldDataStore::getInstance()->getPlayers()[i]->getShape()->setRadius(sqrt(r1 * r1 + r2 * r2));

            qDebug() << "Radius before" << r1 << "Radius after" << GameWorldDataStore::getInstance()->getPlayers()[i]->getShape()->getRadius();

            // Delete the food
            delete GameWorldDataStore::getInstance()->getFoods()[j];
            GameWorldDataStore::getInstance()->getFoods().erase(GameWorldDataStore::getInstance()->getFoods().begin() + j);
            --j;
        }
    }

    qDebug() << "----------------------------------";

    qDebug() << "Id";
    for (auto a : deadPlayersIds)
        qDebug() << a;

    qDebug() << "Index";
    for (auto a : deadPlayersIndices)
        qDebug() << a << GameWorldDataStore::getInstance()->getPlayers()[a]->getShape()->getRadius();

    qDebug() << endl;

    qDebug() << "Size before" << GameWorldDataStore::getInstance()->getPlayers().size() << deadPlayersIndices.size();

    for (int id : deadPlayersIds)
    {
        ConnectionDataStore::getInstance()->getConnections()[id]->getSocket()->disconnect();
        delete ConnectionDataStore::getInstance()->getConnections()[id];

        ConnectionDataStore::getInstance()->getConnections().erase(
                ConnectionDataStore::getInstance()->getConnections().begin() + id);
    }

    for (int idx : deadPlayersIndices)
    {
        delete GameWorldDataStore::getInstance()->getPlayers()[idx];

        GameWorldDataStore::getInstance()->getPlayers().erase(
                GameWorldDataStore::getInstance()->getPlayers().begin() + idx);
    }

    updateIdsAfterDisconnection();
}

void Server::sendDataToClients()
{
    QString seralized = Parser::serializeGameWorld(GameWorldDataStore::getInstance()) + "\n";
    for (Connection *c : ConnectionDataStore::getInstance()->getConnections())
    {
        c->getSocket()->write(("B" + QString::number(c->getId()) + seralized).toStdString().c_str());
    }
}

void Server::timerEvent(QTimerEvent *event)
{
    update();
}

