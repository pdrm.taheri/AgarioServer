//
// Created by pedram on 7/9/17.
//

#ifndef AGARIOSERVER_PARSER_H
#define AGARIOSERVER_PARSER_H


#include <utility>
#include <sstream>
#include <QtCore/QByteArray>
#include "../Model/GameWorld/GameWorld.h"
#include "../Model/GameWorld/GameWorldDataStore.h"

#define DELIMITER "&"

class Parser
{
public:
    static std::pair<int, int> praseMessageFromByteArray(QByteArray &byteArray);
    static QString serializeGameWorld(GameWorldDataStore *world);
};


#endif //AGARIOSERVER_PARSER_H
