//
// Created by pedram on 3/14/17.
//

#ifndef AGARIOSERVER_SERVER_H
#define AGARIOSERVER_SERVER_H


#include <QtNetwork/QTcpServer>
#include <QtNetwork/QHostAddress>
#include <QTimer>
#include <QTimerEvent>
#include <QtCore>
#include "Connection/ConnectionHandler.h"
#include "Connection/Connection.h"
#include "../Constants.h"
#include "../Model/Connection/ConnectionDataStore.h"
#include "../Model/GameWorld/GameWorldDataStore.h"
#include "Parser.h"

class Server : public QObject
{

public:
    static void onNewMessage(int senderId, QByteArray message);
    static void onNewClientConnect(Connection *newConnection);
    static void onClientDisconnected(Connection *connection);

    static Server *getInstance();
    static void run();

    void timerEvent(QTimerEvent *event);

    static void update();

private:
    Server();
    ~Server();

    static void updateIdsAfterDisconnection();
    static void updatePlayersDynamics(int playerId, std::pair<int, int> mouse);
    static void updatePlayersPositions(int playerId);
    static void calculateDeathsAndUpdateWeights();

    static void sendDataToClients();
};


#endif //AGARIOSERVER_SERVER_H
