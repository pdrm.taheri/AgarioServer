//
// Created by pedram on 3/14/17.
//

#include "ConnectionHandler.h"

// Definition of static variables
static ConnectionHandler *instance;

ConnectionHandler *ConnectionHandler::getInstance()
{
    if (instance == nullptr)
    {
        instance = new ConnectionHandler();
    }

    return instance;
}

ConnectionHandler::ConnectionHandler()
{
    tcpServer = new QTcpServer();
    connect(tcpServer, &QTcpServer::newConnection, this, &ConnectionHandler::onNewConnection);
}

ConnectionHandler::~ConnectionHandler()
{
    delete instance;
    delete tcpServer;
}

void ConnectionHandler::onNewWriteOnSocket(Connection *socket)
{
    getInstance()->onNewMessageHandler(socket->getId(), socket->getSocket()->readLine());
    socket->getSocket()->readAll();
}

void ConnectionHandler::onNewConnection()
{
    Connection *connection = new Connection(getInstance()->tcpServer->nextPendingConnection());
    connection->setOnNewBytesToReadHandler(onNewWriteOnSocket);
    connection->setOnConnectionClosedHandler(onSocketClosed);
    getInstance()->onNewConnectionHandler(connection);
}

void ConnectionHandler::setOnNewMessageHandler(void (*handler)(int, QByteArray))
{
    qDebug() << "New message handler set";
    onNewMessageHandler = handler;
}

void ConnectionHandler::setOnNewConnectionHandler(void (*handler)(Connection *))
{
    qDebug() << "New connection handler set";
    onNewConnectionHandler = handler;
}

void ConnectionHandler::listenOn(QHostAddress &hostAddress, unsigned short port)
{
    if (tcpServer->listen(hostAddress, port))
    {
        qDebug() << "Server started";
    }
    else
    {
        qDebug() << "Server failed to start";
    }
}

void ConnectionHandler::onSocketClosed(Connection *socket)
{
    getInstance()->onConnectionClosedHandler(socket);
    qDebug() << "Connection about to close: " << socket->getId();
}

void ConnectionHandler::setOnConnectionClosedHandler(void (*handler)(Connection *))
{
    qDebug() << "Connection about to close handler set";
    onConnectionClosedHandler = handler;
}
