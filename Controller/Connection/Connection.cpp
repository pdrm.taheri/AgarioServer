//
// Created by pedram on 3/14/17.
//

#include "Connection.h"

Connection::Connection(QTcpSocket *socket) : socket(socket)
{
    connect(this->socket, &QTcpSocket::readyRead, this, &Connection::onNewBytesToRead);
    connect(this->socket, &QTcpSocket::disconnected, this, &Connection::onConnectionClosed);
}

Connection::~Connection()
{
}

void Connection::onNewBytesToRead()
{
    onNewBytesToReadHandler(this);
}

void Connection::onConnectionClosed()
{
    onConnectionClosedHandler(this);
}

int Connection::getId() const
{
    return id;
}

QTcpSocket *Connection::getSocket()
{
    return socket;
}

void Connection::setId(int id)
{
    this->id = id;
}

void Connection::setOnConnectionClosedHandler(void (*handler)(Connection *))
{
    this->onConnectionClosedHandler = handler;
}

void Connection::setOnNewBytesToReadHandler(void (*handler)(Connection *))
{
    this->onNewBytesToReadHandler = handler;
}

