//
// Created by pedram on 7/9/17.
//

#include "Parser.h"

std::pair<int, int> Parser::praseMessageFromByteArray(QByteArray &byteArray)
{
    std::istringstream f(byteArray.toStdString());

    std::pair<int, int> coordinate;
    std::string tmp;

    getline(f, tmp, ' ');
    coordinate.first = std::stoi(tmp);

    getline(f, tmp, ' ');
    coordinate.second = std::stoi(tmp);

    return coordinate;
}


// Format:X Y Size[OPTIONAL] Color[OPTIONAL]DELIMITER
QString Parser::serializeGameWorld(GameWorldDataStore *world)
{
    QString serialized = "&";
    for (Player *p : world->getPlayers())
    {
        if (p->getId() != -1)
        {
            QPointF location = p->getShape()->getCentre();
            serialized += QString::number((int) location.x()) + " " + QString::number((int) location.y()) + " ";

            serialized += QString::number((int) p->getShape()->getRadius()) + " ";

            serialized += QString::number(p->getId()) + " ";

            serialized += DELIMITER;
        }
        else
        {
            QPointF location = p->getShape()->getCentre();
            serialized += QString::number((int) location.x()) + " " + QString::number((int) location.y()) + " ";

            serialized += DELIMITER;
        }
    }

    for (Food *f : world->getFoods())
    {
        QPointF location = f->getShape()->getCentre();
        serialized += QString::number((int) location.x()) + " " + QString::number((int) location.y()) + " ";

        serialized += DELIMITER;
    }

    return serialized;
}
