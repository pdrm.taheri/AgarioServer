//
// Created by pedram on 6/29/17.
//

#include "Player.h"

Player::Player(int id) : Food(), id(id)
{
    getShape()->setRadius(25);
    boost = 1;
    brake = 1;
}

Player::~Player()
{

}

int Player::getId() const
{
    return id;
}

void Player::setId(int newId)
{
    id = newId;
}

QString Player::getName() const
{
    return name;
}

void Player::setName(QString newName)
{
    name = newName;
}

//Vector Player::getAcceleration() const
//{
//    return acceleration;
//}
//
//void Player::setAcceleration(Vector newAcceleration)
//{
//    acceleration = newAcceleration;
//}

Vector Player::getSpeed() const
{
    return speed;
}

void Player::setSpeed(Vector newSpeed)
{
    speed = newSpeed;
}

void Player::move()
{
//    acceleration.setX(acceleration.getX() * 0.8);
//    acceleration.setY(acceleration.getY() * 0.8);
//
//

    speed.setX(speed.getX() * brake);
    speed.setY(speed.getY() * brake);

    speed.setX(speed.getX() * boost);
    speed.setY(speed.getY() * boost);

    if (getShape()->getCentre().x() + speed.getX() > 2000 || getShape()->getCentre().x() + speed.getX() < 0 ||
            getShape()->getCentre().y() + speed.getY() > 2000 || getShape()->getCentre().y() + speed.getY() < 0)
    {
        return;
    }

    getShape()->moveBy(speed.getX(), speed.getY());

    speed.setX(speed.getX() / boost);
    speed.setY(speed.getY() / boost);

    boost = ((boost * 0.97 > 1) ? (boost * 0.97) : 1);

}
