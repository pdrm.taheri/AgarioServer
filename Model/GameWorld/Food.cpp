//
// Created by pedram on 6/29/17.
//

#include "Food.h"

Circle *Food::getShape()
{
    return shape;
}

Food::Food()
{
    shape = new Circle();
    shape->setRadius(10);
}

Food::~Food()
{
    delete shape;
}
