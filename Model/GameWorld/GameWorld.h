//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_GAMEWORLD_H
#define AGARIOCLIENT_GAMEWORLD_H


#include <vector>
#include "GameObject.h"
#include "Food.h"
#include "Player.h"

class GameWorld
{
public:
    GameWorld();
    ~GameWorld();

private:
    std::vector<GameObject *> objects;
    std::vector<Food *> foods;
    std::vector<Player *> players;

    Player *me;
};


#endif //AGARIOCLIENT_GAMEWORLD_H
