//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_PLAYER_H
#define AGARIOCLIENT_PLAYER_H

#include <QtCore/QString>

#include "Food.h"
#include "../Geometry/Vector.h"

class Player : public Food
{
public:
    Player(int id);
    ~Player();

    int getId() const;
    void setId(int newId);

    QString getName() const;
    void setName(QString newName);

    Vector getAcceleration() const;
    void setAcceleration(Vector newAcceleration);

    Vector getSpeed() const;
    void setSpeed(Vector newSpeed);

    void move();

    double boost;
    double brake;
private:
    int id;
    QString name;

    Vector acceleration;
    Vector speed;
};


#endif //AGARIOCLIENT_PLAYER_H
