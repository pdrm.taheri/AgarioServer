//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_FOOD_H
#define AGARIOCLIENT_FOOD_H

#include "GameObject.h"
#include "../Geometry/Circle.h"

class Food : public GameObject
{
public:
    Food();
    ~Food();

    virtual Circle *getShape();

private:
    Circle *shape;
};


#endif //AGARIOCLIENT_FOOD_H
