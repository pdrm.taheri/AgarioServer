//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_GAMEOBJECT_H
#define AGARIOCLIENT_GAMEOBJECT_H


#include <QtWidgets/QAbstractGraphicsShapeItem>
#include "../Geometry/Shape.h"
#include "../Geometry/Point.h"

class GameObject
{
public:
    virtual Shape *getShape() = 0;
};


#endif //AGARIOCLIENT_GAMEOBJECT_H
