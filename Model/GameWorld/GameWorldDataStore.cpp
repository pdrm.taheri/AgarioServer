//
// Created by pedram on 3/14/17.
//

#include "GameWorldDataStore.h"

// Definition of static variables
static GameWorldDataStore *instance;

GameWorldDataStore *GameWorldDataStore::getInstance()
{
    if (instance == nullptr)
    {
        instance = new GameWorldDataStore();
    }
    return instance;
}

std::vector<Player *> &GameWorldDataStore::getPlayers()
{
    return activePlayers;
}

GameWorldDataStore::GameWorldDataStore()
{
}

GameWorldDataStore::~GameWorldDataStore()
{
    delete instance;
}

std::vector<Food *> &GameWorldDataStore::getFoods()
{
    return foods;
}
