//
// Created by pedram on 3/14/17.
//

#ifndef CHATROOMSERVER_USERSDATASTORE_H
#define CHATROOMSERVER_USERSDATASTORE_H


#include <vector>
#include "Player.h"

class GameWorldDataStore
{
public:
    static GameWorldDataStore *getInstance();
    std::vector<Player *> &getPlayers();
    std::vector<Food *> &getFoods();

private:
    GameWorldDataStore();
    ~GameWorldDataStore();

    std::vector<Player *> activePlayers;
    std::vector<Food *> foods;
};


#endif //CHATROOMSERVER_USERSDATASTORE_H
