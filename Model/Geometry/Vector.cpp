//
// Created by pedram on 6/29/17.
//

#include <cmath>
#include "Vector.h"

Vector::Vector(const double x, const double y) : x(x), y(y)
{
    calculateLength();
}

Vector::Vector() : x(0), y(0) { calculateLength(); }

Vector::~Vector() { }

void Vector::setX(double x)
{
    this->x = x;
    calculateLength();
}

void Vector::setY(double y)
{
    this->y = y;
    calculateLength();
}

void Vector::calculateLength()
{
    this->length = sqrt(x * x + y * y);
}

double Vector::getX() const
{
    return x;
}

double Vector::getY() const
{
    return y;
}

double Vector::getLength() const
{
    return length;
}

Vector Vector::normalized() const
{
    return Vector(x * 2 / length, y * 2 / length);
}
