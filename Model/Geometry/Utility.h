//
// Created by Pedram Taheri on 7/15/2017 AD.
//

#ifndef AGARIOSERVER_UTILITY_H
#define AGARIOSERVER_UTILITY_H


#include "../GameWorld/Player.h"

class Utility
{
public:
    static Player *canEat(Player *p1, Player *p2); // Returns the eater!
    static bool canEat(Player *p, Food *f); // Returns the eater!
};


#endif //AGARIOSERVER_UTILITY_H
