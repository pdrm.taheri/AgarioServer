//
// Created by Pedram Taheri on 7/15/2017 AD.
//

#include "Utility.h"

Player *Utility::canEat(Player *p1, Player *p2)
{
    double r1 = p1->getShape()->getRadius();
    double r2 = p2->getShape()->getRadius();

    // Similar sizes cannot eat each other
    if (r1 < r2 && 1.5 * r1 > r2)
        return nullptr;
    if (r2 < r1 && r1 < 1.5 * r2)
        return nullptr;

    double deltaX = abs((int) (p1->getShape()->getCentre().x() - p2->getShape()->getCentre().x()));
    double deltaY = abs((int) (p1->getShape()->getCentre().y() - p2->getShape()->getCentre().y()));
    double distance = Vector(deltaX, deltaY).getLength();

    if (r1 > r2 && distance < r1)
        return p1;
    else if (r2 > r1 && distance < r2)
        return p2;

    return nullptr;
}

bool Utility::canEat(Player *p, Food *f)
{
    double deltaX = abs((int) (p->getShape()->getCentre().x() - f->getShape()->getCentre().x()));
    double deltaY = abs((int) (p->getShape()->getCentre().y() - f->getShape()->getCentre().y()));
    double distance = Vector(deltaX, deltaY).getLength();

    return distance < p->getShape()->getRadius();
}
