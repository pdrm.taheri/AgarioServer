//
// Created by pedram on 6/29/17.
//

#include "Shape.h"

QPointF &Shape::getCentre()
{
    return centre;
}

void Shape::setCentre(QPointF newCentre)
{
    centre = newCentre;
}
