//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_CIRCLE_H
#define AGARIOCLIENT_CIRCLE_H


#include "Shape.h"

class Circle : public Shape
{
public:
    Circle();
    ~Circle();

    void moveBy(double x, double y);

    double getRadius() const;
    void setRadius(double newRadius);

private:
    double radius;
};


#endif //AGARIOCLIENT_CIRCLE_H
