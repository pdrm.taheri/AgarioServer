//
// Created by pedram on 6/29/17.
//

#include <cstdlib>
#include "Circle.h"

Circle::Circle()
{
    setCentre(QPointF(rand() % 2000, rand() % 2000));
    radius = 30;
}

Circle::~Circle()  { }

void Circle::moveBy(double x, double y)
{
    getCentre().setX(getCentre().x() + x);
    getCentre().setY(getCentre().y() + y);
}

double Circle::getRadius() const
{
    return radius;
}

void Circle::setRadius(double newRadius)
{
    radius = newRadius;
}


