//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_SHAPE_H
#define AGARIOCLIENT_SHAPE_H


#include <QtCore/QPointF>

class Shape
{
public:
    virtual void moveBy(double x, double y) = 0;

    QPointF &getCentre();
    void setCentre(QPointF newCentre);

private:
    QPointF centre;
};


#endif //AGARIOCLIENT_SHAPE_H
