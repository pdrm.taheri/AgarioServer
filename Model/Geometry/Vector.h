//
// Created by pedram on 6/29/17.
//

#ifndef AGARIOCLIENT_VECTOR_H
#define AGARIOCLIENT_VECTOR_H


class Vector
{
public:
    Vector(const double x, const double y);
    Vector();
    ~Vector();

    void setX(double x);
    void setY(double y);

    double getX() const;
    double getY() const;
    double getLength() const;

    Vector normalized() const;

private:
    void calculateLength();

    double x;
    double y;
    double length;
};


#endif //AGARIOCLIENT_VECTOR_H
