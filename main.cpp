//
// Created by pedram on 6/29/17.
//

#include <QtWidgets/QApplication>
#include <QtCore>
#include "Controller/Server.h"



int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Server::run();

    return app.exec();
}
